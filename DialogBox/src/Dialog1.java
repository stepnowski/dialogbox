import javax.swing.JOptionPane;

public class Dialog1
{

	public static void main(String[] args) 
	{
		JOptionPane.showMessageDialog(null, "Welcome to this dialog box example");
		//prompt user to enter name
		String name = JOptionPane.showInputDialog("What is your name?");
		
		String message = String.format("Welcome, %s, this is my webpage!", name);
		
		JOptionPane.showMessageDialog(null, message);
	}

}
